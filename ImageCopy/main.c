#include <stdio.h>
#include <stdlib.h>

int main (){
    
    /* Read file and store in the streamIn */
    FILE *streamIn = fopen("images/cameraman.bmp", "rb");

    /* Store the copy of streamIn in the copyOut file */
    FILE *copyOut = fopen("images/cameraman_copy.bmp", "wb");

    if(streamIn == (FILE*)0){
        printf("Unable to open file\n");
    }
    
    /* Buffer to store image header and body from streamIn */
    unsigned char header[54];
    unsigned char colorTable[1024];

    for (int i = 0; i < 54; i++){
        /* Get 1 byte from streamIn */
        header[i] = getc(streamIn);
    }
    
    /* Read the image widht, height and bitDepth from header part with some offsets (18,22,28) */
    int width = *(int *)&header[18];
    int height = *(int *)&header[22];
    int bitDepth = *(int *)&header[28];

    /* If image has Color table we need to read */
    if (bitDepth <= 8){
        fread(colorTable, sizeof(unsigned char), 1024, streamIn);
    }

    /* write the header information to the new output image */
    fwrite(header, sizeof(unsigned char), 54, copyOut);
    
    /* buffer to store image data from streamIn */
    unsigned char buf[height * width];
    fread(buf, sizeof(unsigned char), (height*width), streamIn);

    /* Write color table if exists */
    if (bitDepth <= 8){
        fwrite(colorTable, sizeof(unsigned char), 1024, copyOut);
    }
    
    /* Then write the pixel data store in the buffer */ 
    fwrite(buf, sizeof(unsigned char), (height*width), copyOut);
    
    /* close the stream data */ 
    fclose(copyOut);
    fclose(streamIn);

    printf("Success !\n");
    printf("Width: %d\n", width);
    printf("Height: %d\n", height);

    return 0;
}