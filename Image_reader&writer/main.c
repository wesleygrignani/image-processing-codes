/**
 * @file main.c
 * @author Wesley Grignani (wesley.grignani@edu.univali.br)
 * @brief C Code for read and write a BMP image format 
 * @version 0.1
 * @date 2022-08-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <stdio.h>
#include <stdlib.h>

/* Image informations (BMP format) */
#define BMP_HEADER_SIZE        54
#define BMP_COLOR_TABLE_SIZE   1024
#define CUSTOM_IMG_SIZE        1024*1024

/* Read image function */
void imageReader(const char *imgName,
                 int *_height,
                 int *_width,
                 int *_bitDepth,
                 unsigned char *_header,
                 unsigned char *_colorTable,
                 unsigned char *_buf);

/* Write image function */
void imageWriter(const char *imgName,
                 unsigned char *header,
                 unsigned char *colorTable,
                 unsigned char *buf,
                 int bitDepth);


int main (){
    
    /* Create image variables based on defines */
    int imgWidth, imgHeight, imgBitDepth;
    unsigned char imgHeader[BMP_HEADER_SIZE];
    unsigned char imgColorTable[BMP_COLOR_TABLE_SIZE];
    unsigned char imgBuffer[CUSTOM_IMG_SIZE];

    /* Store image input name and store name */
    const char imgName[] = "images/man.bmp";
    const char newImgName[] = "images/man_copy.bmp";

    /* call read and write function */
    imageReader(imgName, &imgHeight, &imgWidth, &imgBitDepth, &imgHeader[0], &imgColorTable[0], &imgBuffer[0]);
    imageWriter(newImgName, imgHeader, imgColorTable, imgBuffer, imgBitDepth);

    printf("Success !!\n");

    return 0;
}

/**
 * @brief 
 * 
 * @param imgName 
 * @param _height 
 * @param _width 
 * @param _bitDepth 
 * @param _header 
 * @param _colorTable 
 * @param _buf 
 */
void imageReader(const char *imgName,
                 int *_height,
                 int *_width,
                 int *_bitDepth,
                 unsigned char *_header,
                 unsigned char *_colorTable,
                 unsigned char *_buf)
{
    int i;
    /* Open the image in streamIn */
    FILE *streamIn;
    streamIn = fopen(imgName,"rb");

    if (streamIn == (FILE*)0)
    {
        printf("Unable to read image\n");
    }

    /* Read the header information from the image */
    for (i = 0; i < 54; i++)
    {
        _header[i] = getc(streamIn);
    }
    
    /* Get width, height and bitDepth form header image */
    *_width = *(int *)&_header[18];
    *_height = *(int *)&_header[22];
    *_bitDepth = *(int *)&_header[28];

    /* Read colorTable if exists */
    if (*_bitDepth <= 8)
    {
        fread(_colorTable, sizeof(unsigned char), 1024, streamIn);
    }
    
    /* Read pixels data */
    fread(_buf, sizeof(unsigned char), CUSTOM_IMG_SIZE, streamIn);
    
    fclose(streamIn);

}

/**
 * @brief 
 * 
 * @param imgName 
 * @param header 
 * @param colorTable 
 * @param buf 
 * @param bitDepth 
 */
void imageWriter(const char *imgName,
                 unsigned char *header,
                 unsigned char *colorTable,
                 unsigned char *buf,
                 int bitDepth)
{   
    /* Open or Create file to save image */
    FILE *fo = fopen(imgName,"wb");
    /* Write the header stream */
    fwrite(header, sizeof(unsigned char), 54, fo);
    
    /* Write colorTable if exists */
    if (bitDepth <= 8)
    {
        fwrite(colorTable, sizeof(unsigned char), 1024, fo);
    }

    /* Write the pixels data from buffer */
    fwrite(buf, sizeof(unsigned char), CUSTOM_IMG_SIZE, fo);

    fclose(fo);
    
}