/**
 * @file main.c
 * @author Wesley Grignani (wesley.grignani@edu.univali.br)
 * @brief 
 * @version 0.1
 * @date 2022-08-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include <stdio.h>
#include <stdlib.h>

int main(){
    
    FILE *FIn = fopen("images/lena_color.bmp", "rb");
    FILE *Fout = fopen("images/lena_gray.bmp", "wb");

    unsigned char imgHeader[54];
    unsigned char colorTable[1024];

    /* Check image was read correctly */
    if(FIn == NULL){
        printf("Unable to open image. \n");
        return -1;
    }

    /* Read the header part of image file */
    for (int i = 0; i < 54; i++)
    {
        imgHeader[i] = getc(FIn);
    }
    /* Write header part to output image */
    fwrite(imgHeader, sizeof(unsigned char), 54, Fout);
    
    /* Read height, width and bitDepth of image */
    int width = *(int *)&imgHeader[18];
    int height = *(int *)&imgHeader[22];
    int bitDepth = *(int *)&imgHeader[28];

    /* checking if image has colorTable */
    if (bitDepth <= 8)
    {
        fread(colorTable, sizeof(unsigned char), 1024, FIn);
        fwrite(colorTable, sizeof(unsigned char), 1024, Fout);
    }
    
    /* Calc the image size */
    int imgSize = height * width;
    unsigned char buffer[imgSize][3];

    /* Read the 3 color channels */
    for (int i = 0; i < imgSize; i++)
    {
        buffer[i][0] = getc(FIn); // Red  
        buffer[i][1] = getc(FIn); // Green  
        buffer[i][2] = getc(FIn); // Blue  

        int temp = 0;

        /* Graysclae conversion methodd */
        temp = (buffer[i][0]*0.3) + (buffer[i][1]*0.59) + (buffer[i][2]*0.11);

        /* Write to output stream */
        putc(temp, Fout);
        putc(temp, Fout);
        putc(temp, Fout);
    }   
    
    printf("Success !\n");
    fclose(FIn);
    fclose(Fout);
    return 0;
}